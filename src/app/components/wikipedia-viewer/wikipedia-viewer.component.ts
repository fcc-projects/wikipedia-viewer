import { Component, OnInit } from '@angular/core';
import { WikipediaService } from '../../services/wikipedia.service';

@Component({
  selector: 'app-wikipedia-viewer',
  templateUrl: './wikipedia-viewer.component.html',
  styleUrls: ['./wikipedia-viewer.component.scss']
})
export class WikipediaViewerComponent implements OnInit {
  query = '';
  entries: any = [];
  constructor(private wikipediaService: WikipediaService) { }

  ngOnInit() {
  }
  getEntries() {
    this.entries = [];
    if (this.query.length > 0) {
      this.wikipediaService.searchWiki(this.query)
        .subscribe(entries => {
          const articles = entries.query.pages;
          for (const key in  articles) {
            if (articles.hasOwnProperty(key)) {
              this.entries.push({ title: articles[key]['title'],
                                  text: articles[key]['extract'],
                                  url: articles[key]['fullurl'] })
            }
          }
          console.log(this.entries);
        })
    }
  }

}
